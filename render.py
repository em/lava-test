#!/usr/bin/env python3

import sys
import yaml

log = yaml.safe_load(open(sys.argv[1]))
fails = 0
for line in log:
    icons = {
        "pass": "✅",
        "fail": "🛑",
    }
    d = line["msg"] if line["lvl"] == "results" else {}
    result = d.get("result")
    if result == "fail":
        fails += 1
    icon = icons.get(result, "  ")
    print(line["dt"], line["lvl"].ljust(8), icon, line["msg"])

sys.exit(fails != 0)
